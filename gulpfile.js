var gulp = require('gulp'),
    connect = require('gulp-connect'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    changed = require('gulp-changed'),
    cssnano = require('cssnano'),
    sourcemaps = require('gulp-sourcemaps');




gulp.task('scss', function() {
    gulp.src(['./app/style/scss/all.scss'])
        .pipe(plumber({errorHandler: reportError}))
        .pipe(changed('./app/style/css/'))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(concat('main.css'))
        .pipe(gulp.dest('./app/style/css/'))
        .pipe(postcss([
            autoprefixer({ browsers: ['last 5 versions'] }),cssnano
        ]))
        .pipe(rename({suffix: '-min'}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./app/style/css/'));
});


gulp.task('webserver', function() {
    connect.server({
        port: 55555,
        root: 'app',
        livereload: false
    });
});



gulp.task('watch', function() {
    gulp.watch('./app/style/scss/**/*.*', ['scss']);
});


gulp.task('default', function () {
    gulp.start('webserver', 'watch');
});

//======================================================================================================================

var reportError = function (error) {
    notify({
        title: 'Error',
        message: 'Check the console.'
    }).write(error);

    console.log(error.toString());

    this.emit('end');
};