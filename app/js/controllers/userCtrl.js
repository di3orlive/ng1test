define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('userCtrl', ['$scope', '$stateParams', 'usersService', function ($scope, $stateParams, usersService) {
        $scope.userId = $stateParams.id;
        $scope.isDuncanMacLeod = $stateParams.isDuncanMacLeod === 'true';

        $scope.user = {};
        $scope.user = usersService[$scope.userId];
    }]);
});
