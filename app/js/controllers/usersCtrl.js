define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('usersCtrl', ['$scope', 'usersService', function ($scope, usersService) {
        $scope.users = [];
        $scope.users = usersService;

        
        /* sort users */
        $scope.prop = null;
        $scope.searchWord = '';

        $scope.sortBy = function(prop) {
            $scope.prop = prop;
        };
    }]);
});
