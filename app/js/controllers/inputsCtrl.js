define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('inputsCtrl', function ($scope) {
        /* first box */
        $scope.num = {};
        $scope.num.one = 100;
        $scope.num.two = 100;
        $scope.num.three = 100;

        $scope.calc = function() {
            $scope.num.four = +$scope.num.one + +$scope.num.two + +$scope.num.three;
        };

        $scope.average = function() {
            $scope.num.one = $scope.num.four * 0.5;
            $scope.num.two = $scope.num.four * 0.25;
            $scope.num.three = $scope.num.four * 0.25;
        };
        

        $scope.calc();
        
        
        /* second box */
        $scope.cart = {
            one: null,
            two: null,
            ree: null
        };
    });
});
