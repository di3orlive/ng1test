define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('userAboutCtrl', ['$scope', '$stateParams', 'usersService', function ($scope, $stateParams, usersService) {
        $scope.userId = $stateParams.id;

        $scope.user = {};
        $scope.user = usersService[$scope.userId];
    }]);
});
