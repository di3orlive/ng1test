define(['./app'], function (app) {
    'use strict';

    return app.config(function($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide) {
    
        app.lazy = {
            controller: $controllerProvider.register,
            directive: $compileProvider.directive,
            filter: $filterProvider.register,
            factory: $provide.factory,
            service: $provide.service
        };

        $urlRouterProvider.otherwise("/home");

        $stateProvider
            .state('home', {
                url: "/home",
                templateUrl: "views/home.html",
                controller: 'homeCtrl'
            })
            .state('inputs', {
                url: "/inputs",
                templateUrl: "views/inputs.html",
                controller: 'inputsCtrl'
            })
            .state('users', {
                url: "/users",
                templateUrl: "views/users.html",
                controller: 'usersCtrl'
            })
            .state('usersInfo', {
                url: "/user/info/:id?isDuncanMacLeod",
                views: {
                    '': { 
                        templateUrl: 'views/user.info.html', 
                        controller: 'userCtrl'
                    },
                    'about@usersInfo': { 
                        templateUrl: 'views/user.about.html',
                        controller: 'userAboutCtrl',
                        resolve: {
                            deps: function ($q, $rootScope) {
                                var deferred = $q.defer();

                                var dependencies = [
                                    'controllers/userAboutCtrl',
                                ];

                                require(dependencies, function() {
                                    $rootScope.$apply(function() {
                                        deferred.resolve();
                                    });
                                });

                                return deferred.promise;
                            }
                        }
                    }
                }
        });
    });
});

