/**
 * loads sub modules and wraps them up into the main module
 * this should be used for top-level module definitions only
 */
define([
    'angular',
    'uiRouter',
    'angular-animate',
    './controllers/index',
    './directives/index',
    './filters/index',
    './services/index'
], function (angular) {
    'use strict';

    return angular.module('app', [
        'ui.router', 
        'ngAnimate',
        'app.controllers',
        'app.directives',
        'app.filters',
        'app.services'
    ]);
});
