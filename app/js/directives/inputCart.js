define(['./module'], function (controllers) {
    'use strict';
    controllers.directive('inputCart', function () {
        return {
            restrict: 'A',
            scope: {
                ngModel : '=',
            },
            link: function($scope, $element, $attrs){

                // function rec(curInput){
                //      if (curInput != null && curInput.previousElementSibling != null){
                //          console.log(1);
                //          rec(curInput.previousElementSibling);
                //     }else{
                //         while (curInput.nextElementSibling.value.length == 0) {
                //             if (curInput.tagName.toLowerCase() == "input") {
                //                 curInput.focus();
                //                 break;
                //             }
                //         }
                //     }
                // }


                $element.bind('keydown', function($event) {
                    var firstInput = angular.element($element)[0].children[0];
                    var curInput = $event.target;

                    
                    if (curInput.value.length >= 5) {
                        while (curInput = curInput.nextElementSibling) {
                            if (curInput.tagName.toLowerCase() == "input") {
                                curInput.focus();
                                break;
                            }
                        }
                    }else if (curInput != firstInput && curInput.value.length == 0){
                        while (curInput = curInput.previousElementSibling) {
                            if (curInput.tagName.toLowerCase() == "input") {
                                curInput.focus();
                                break;
                            }
                        }
                    }

                    
                    if(curInput != null && curInput.previousElementSibling != null){
                        while(curInput.previousElementSibling.value.length < 5){
                            curInput.previousElementSibling.value = curInput.previousElementSibling.value + curInput.value;
                            curInput.value = '';
                            break;
                        }
                    }
                });
            }
        };
    });
});
