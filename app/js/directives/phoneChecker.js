define(['./module'], function (controllers) {
    'use strict';
    // controllers.directive('phoneChecker', function() {
    //     return {
    //         require: 'ngModel',
    //         scope: {model:'=ngModel'},
    //         link: function($scope, $element, $attrs, ngModelCtrl) {
    //             var input = $scope.ngModel + '';
    //             $scope.$watch('model', function (newVal) {
    //                 // console.log(newVal);
    //                 input = newVal ? newVal : '';
    //             });


               
    //             function check(x){
    //                 var country, city, number, regexObj;
    //                 regexObj = /^(?:\+?1[-. ]?)?(?:\(?([0-9]{3})\)?[-. ]?)?([0-9]{3})[-. ]?([0-9]{4})$/;
                  

    //                 console.log(input.length);
    //                 if(event.keyCode == 8){
    //                     return input.slice(0, -1);
    //                 }

    //                 if (regexObj.test(input.value)) {
    //                     var parts = input.value.match(regexObj);
    //                     var phone = "";

    //                     if (parts[1]) {
    //                         phone += "(" + parts[1] + ") ";
    //                     }
    //                     phone += parts[2] + "-" + parts[3];
    //                     return phone;
    //                 } else {
    //                     //invalid phone number
    //                     return input;
    //                 }
    //             }


               
                
    //             $element.bind('keydown', function(event) {
    //                 this.value = check(this);
    //             });




    controllers.directive('phoneChecker', function($filter, $browser) {
    return {
        require: 'ngModel',
        scope: {ngModel:'='},
        link: function($scope, $element, $attrs, ngModelCtrl) {
            var listener = function() {
                var value = $element.val().replace(/[^0-9]/g, '');
                $element.val($filter('tel')(value, false));
            };

            // This runs when we update the text field
            ngModelCtrl.$parsers.push(function(viewValue) {
                return viewValue.replace(/[^0-9]/g, '').slice(0,10);
            });

            // This runs when the model gets updated on the scope directly and keeps our view in sync
            ngModelCtrl.$render = function() {
                $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
            };

            $element.bind('change', listener);
            $element.bind('keydown', function(event) {
                var key = event.keyCode;
                // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                // This lets us support copy and paste too
                if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
                    return;
                }
                $browser.defer(listener); // Have to do this or changes don't get picked up properly
            });

            $element.bind('paste cut', function() {
                $browser.defer(listener);
            });
        }

    };
});

});


